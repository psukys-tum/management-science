# Linear Programming - Modelling

Analytics disciplines:

- descriptine - what happened
- diagnostic - why did it happen
- predictive - what will happen
- prescriptibe - how can we make it happen

## Optimization

**Optimization** - make something best and prove that nothing else *will be better*. The following ingredients allow forming/shaping the program.

**Objective function** (all about the target):
> *make it best* depends on the target

- maximize the contribution margin in a supply chain
- minimize the inventory holding costs in a supply chain

**Decision variables** (degrees of freedom):
> to reach the target, there have to be some degrees of freedom

- production amount of a given product
- amount of stored products in inventory

**Constraints** (limitation of the degree of freedom):
> freedom of my decision may be restricted

- Production capacities of machines
- Minimum and maximum inventory levels that mustn't be violated


All kinds of type of decision variables - *binary, integer, float*

Constraints in general are inequalities
