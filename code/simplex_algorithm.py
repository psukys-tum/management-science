"""
Prerequisites:
    - non negative constraint
    - maximum constraint
    - maximize/minimze some function

Identification:
    - As many *slack variables* as there are constraints (non-negativity excluded)
    - *non-basic variables* as many as there are variables
    - only one unique slack variable is added to every constraint

Tableau format:
    [non_basic_variables] [slack_variables] [target_function]

For example:
    Maximize: z = 8x_1 + 10x_2 + 7x_3
    Subject to:
        (1) x_1 + 3x_2 +2x_3 <= 10
        (2) x_1 + 5x_2 + x_3 <= 8
        x_1, x_2, x_3 >= 0

    Two constraints: (1), (2) -> two slack variables
    Three variables: x_1, x_2, x_3 -> three non-basic variables

    Move all term to the side of z:
        -8x_1 - 10x_2 - 7x_3 + z = 0

    Tableau:
        |   x_1    |   x_2  |   x_3 |   s_1 |   s_2 |   z   |   c   
        -------------------------------------------------------------
            1           3       2       1       0       0   |   10
            1           5       1       0       1       0   |   8
        -------------------------------------------------------------
            -8          -10     -7      0       0       1   |   0

    Not optimal, because there are negative entries (last row): apply pivot operation.

    Pivot operation on column which has the most negative value in table.

    Come up with corresponding row ratios for constants (last column):

    |   x_1    |   x_2  |   x_3 |   s_1 |   s_2 |   z   |   c   
    -------------------------------------------------------------
        1      |    3   |   2       1       0       0   |   10      10/3 = 3 1/3
        1      |    5   |   1       0       1       0   |   8       8/5 = 1 3/5
    -------------------------------------------------------------
        -8     |    -10 |   -7      0       0       1   |   0

    Lowest ratio indicates which is pivot element. Here it is 5 (2 row), because 8/5 < 10/3.

    Pivot on 5 (2nd row) - set that row's column value to 1 and other same column values - 0:

    |   x_1    |   x_2  |   x_3 |   s_1 |   s_2 |   z   |   c   
    -------------------------------------------------------------
        1           3       2       1       0       0   |   10
        1          [5]      1       0       1       0   |   8
    -------------------------------------------------------------
        -8          -10     -7      0       0       1   |   0
    
    R_2 = R_2 / 5 
    R_1 = R_1 - 3 * R_2
    R_3 =  R_3 + 10 * R_2

    |   x_1    |   x_2  |   x_3 |   s_1 |   s_2 |   z   |   c   
    -------------------------------------------------------------
        2/5         0       7/5     1      -3/5     0   |   26/5
        1/5         1       1/5     0       1/5     0   |   8/5
    -------------------------------------------------------------
        -6          0       -5      0       2       1   |   80/5

    We still have negative entries.
    Repeat the picking by most negative and pivot accordingly:
    -6 most negative:
    |   x_1    |   x_2  |   x_3 |   s_1 |   s_2 |   z   |   c   
    -------------------------------------------------------------
        2/5         0       7/5     1      -3/5     0   |   26/5    26/5 * 5/2 = 13
        1/5         1       1/5     0       1/5     0   |   8/5     8/5 * 5 = 8 
    -------------------------------------------------------------
        -6          0       -5      0       2       1   |   80/5

    8 is smallest thus R2 is pivot row.

    R_2 = 5 * R_2
    R_1 = R_1 - 2/5 * R_2
    R_3 = R_3 + 6 * R_2

    |   x_1    |   x_2  |   x_3 |   s_1 |   s_2 |   z   |   c   
    -------------------------------------------------------------
        0           -2      1       1      -1       0   |   2
        1           5       1       0       1       0   |   8
    -------------------------------------------------------------
        0           30      1       0       8      1   |   64

    no negative entries -> optimal!

    x_2, x_3 and s_2 are not unique (only single value, others have to be 0s) - non-basic.

    x_1 -> basic = 8
    x_2 -> non basic = 0
    x_3 -> non basic = 0
    s_1 -> basic = 2
    s_2 -> non basic = 0
    z -> basic = 64

    Maximum value of 64 at (8, 0, 0)
"""
import sys


class SimplexAlgorithm:
    """ Gives an optimal solution given the constraints and cost function. """

    def __init__(self, constraints=list, cost_function=list) -> None:
        """
        Give initial parameters for Simplex algorithm

        Args:
            constraints: matrix of parameters.
            const_function: cost function of how parameters influence maximization.
            
        Example:
            Given the following constraints:
                x_1 + 2 * x_2 + 10 * x_3 <= 123
                x_2 - x_3 <= 50
            And cost function as:
            [z =] x_1 - x_2 + 10 * x_3
            converted to 
            z - x_1 + x_2 - 10 * x_3 = 0
            
            Constraints should be:
            [[1, 2, 10, 123],
             [0, 1, -1, 50]]

            Cost function should be:
            [1, -1, 1, -10, 0]
        """
        self.constraints = constraints
        self.cost_function = cost_function

    def build_tableau(self) -> None:
        """
        Build a working tableau to solve Simplex method
        """
        
        self.tableau = []
        slack_variables = len(self.constraints)

        # Add constraint rows with pivots
        for slack_index, row in enumerate(self.constraints):
            # exclude last value - constant
            tableau_row = row[:-1]
            
            # Build slack variable columns
            slacks = [0] * slack_variables
            slacks[slack_index] = 1 # in specific location
            slack_index += 1
            
            tableau_row.extend(slacks)
            tableau_row.append(0) # z - cost function value
            tableau_row.append(row[-1]) # add constant
            self.tableau.append(tableau_row)

        # last row - cost function values inverted
        cost_parameters = self.cost_function[1:-1] 
        cost_parameters.extend([0] * slack_variables)
        cost_parameters.append(self.cost_function[0]) # z - cost function value
        cost_parameters.append(self.cost_function[-1]) # constant - actual value
        self.tableau.append(cost_parameters)

    def find_pivot(self) -> tuple:
        """ Find coordinates for the pivot point:
            column is the lowest value  of last row (cost function)
            row is based on dividing constant by the value of that column item and whichever has smallest result

            Returns: tuple (column, row) for pivot value
        """
        column = self.tableau[-1].index(min(self.tableau[-1][:-1]))
        if self.tableau[-1][column] >= 0: # if there are no more negative values - no more pivot points
            return None
        if max([row[column] for row in self.tableau]) <= 0:
            raise Exception('Unbounded solution')

        rows = []
        for row in self.tableau[:-1]:
            if row[column] == 0 or row[-1] / row[column] <= 0:
                rows.append(float('inf')) # https://stackoverflow.com/a/37300370/552214
            else:
                rows.append(row[-1] / row[column])

        return (column, rows.index(min(rows)),)

    def update_pivot_row(self, row=list, column_index=int) -> list:
        """ Updates a row by a given column value to get it to 1
        Args:
            row:    list of values in specific row
            column_index:   index of the column that should be equal to 1 at the end

        Returns:    updated row as a list
        """
        divider = 1 / row[column_index]
        return [item * divider for item in row]

    def update_non_pivot_row(self, row_index=int, column_index=int, pivot_row_index=int) -> None:
        """ Updates non pivot row by subtracting/adding  pivot row to it.
        Args:
            row_index:  index of row that has to be updated
            row_column: index of column that has the pivot value
            pivot_row_index:    index of pivot row
        """
        row = self.tableau[row_index]
        pivot_row = self.tableau[pivot_row_index]
        # Divider - multiplicative difference between two values of same column (different rows)
        divider = row[column_index] / pivot_row[column_index]
        updated_row = [row[idx] - divider * pivot_row[idx] for idx in range(len(row))]
        self.tableau[row_index] = updated_row

    def update_tableau(self, pivot_point=tuple) -> None:
        """Given a pivot point, updates all of the rows so that the pivot point value is 1 and other that column's values are 0
        Args:
            pivot_point: tuple (column, row,)
        """
        # 1. update pivot point's row
        pivot_col, pivot_row = pivot_point
        self.tableau[pivot_row] = self.update_pivot_row(self.tableau[pivot_row], pivot_col)

        # 2. update all the other rows (excluding pivot point's row)
        # up to pivot row
        for row_index in range(pivot_row):
            self.update_non_pivot_row(row_index=row_index, column_index=pivot_col, pivot_row_index=pivot_row)
        # from pivot row
        for row_index in range(pivot_row + 1, len(self.tableau)):
            self.update_non_pivot_row(row_index=row_index, column_index=pivot_col, pivot_row_index=pivot_row)

    def solve_tableau(self):
        """ Solves tableau until there are no pivot points. """
        pivot_point = self.find_pivot()
        while pivot_point:
            self.update_tableau(pivot_point=pivot_point)
            pivot_point = self.find_pivot()