"""Verify that simplex algorithm works as expected."""

import unittest
import timeout_decorator
import math
from simplex_algorithm import SimplexAlgorithm

class TestSimplexAlgorithm(unittest.TestCase):
    """Generic SimplexAlgorithm tests."""
    
    def setUp(self):
        self.constraints = [[2, 1, 18],
                            [2, 3, 42],
                            [3, 1, 24]]
        self.cost_function = [1, -3, -2, 0]
        self.tableau = [[2,   1, 1, 0, 0, 0, 18],
                        [2,   3, 0, 1, 0, 0, 42],
                        [3,   1, 0, 0, 1, 0, 24],
                        [-3, -2, 0, 0, 0, 1, 0]]
        self.obj = SimplexAlgorithm(constraints=self.constraints, cost_function=self.cost_function)
        self.maxDiff = None

    def similar_lists(self, first_list, second_list):
        if len(first_list) != len(second_list):
            print('List lengths differ')
            return False
        for idx in range(len(first_list)):
            if not math.isclose(first_list[idx], second_list[idx]):
                print('{0} and {1} at index {2} are not close'.format(first_list[idx], second_list[idx], idx))
                return False
        return True

    def test_build_tableau(self):
        """ Check that tableau is correctly built."""
        self.obj.build_tableau()
        self.assertEqual(self.obj.tableau, self.tableau)

    def test_find_pivot(self):
        """ Check if pivot is correctly found."""
        self.obj.tableau = self.tableau
        observed = self.obj.find_pivot()
        self.assertEqual(observed, (0, 2,))

    def test_find_pivot_none(self):
        """ Find pivot when there's none. """
        self.tableau[len(self.tableau) - 1] = [0] * len(self.tableau[2])

        self.obj.tableau = self.tableau

        observed = self.obj.find_pivot()
        self.assertEqual(observed, None)

    def test_update_pivot_row(self):
        """ Simple test to check if pivot row is updated. """
        pivot_row = [1, 2, 3, 4, 5, 6, 7, 9]
        pivot_col = 1
        expected_row = [item / 2 for item in pivot_row]
        observed = self.obj.update_pivot_row(row=pivot_row, column_index=pivot_col)
        self.assertEqual(observed, expected_row)

    def test_update_non_pivot_row(self):
        """ Test that corresponding row is updated. """
        self.obj.tableau = self.tableau
        self.obj.tableau[1] = [1, 1, 0, 1, 0, 0, 42]
        self.obj.update_non_pivot_row(row_index=0, column_index=0, pivot_row_index=1)
        observed = self.obj.tableau[0]
        self.assertEqual(observed, [0, -1, 1, -2, 0, 0, -66])

    def test_update_tableau(self):
        """ Check that updating the whole tableau on a given point works out. """
        self.obj.tableau = self.tableau
        pivot_point = (0, 2)
        expected_tableau = [[0, 1/3, 1, 0, -2/3, 0, 2],
                            [0, 7/3, 0, 1, -2/3, 0, 26],
                            [1, 1/3, 0, 0, 1/3, 0, 8],
                            [0, -1, 0, 0, 1, 1, 24]]
        
        self.obj.update_tableau(pivot_point=pivot_point)
        
        for i in range(len(self.obj.tableau)):
            self.assertTrue(self.similar_lists(self.obj.tableau[i], expected_tableau[i]))

    @timeout_decorator.timeout(3)
    def test_solve_tableau_phpsimplex(self):
        """ whole thing with exercise from http://www.phpsimplex.com/en/simplex_method_example.htm. """
        self.obj.build_tableau()
        
        expected_tableau = [[0, 1, -1/2, 1/2, 0, 0, 12],
                            [0, 0, -7/4, 1/4, 1, 0, 3],
                            [1, 0, 3/4, -1/4, 0, 0, 3],
                            [0, 0, 5/4, 1/4, 0, 1, 33]]

        self.obj.solve_tableau()

        for i in range(len(self.obj.tableau)):
            self.assertTrue(self.similar_lists(self.obj.tableau[i], expected_tableau[i]))

    def test_unbound_solution(self):
        """ Test that searching for pivot is capable of identifying unbound solution. """
        self.obj.tableau = [[-1] * 5 for i in range(5)]
        self.assertRaises(Exception, self.obj.find_pivot)
        

if __name__ == "__main__":
    unittest.main()